import React from 'react';
import {
  AppRegistry,
  asset,
  Pano,
  Text,
  View,
  NativeModules
} from 'react-vr';


export default class Wanderlust_VR extends React.Component {
  fetchAssetDirection() {
    const regex = new RegExp("[?&]place(=([^&#]*)|&|#|$)");
    const results = regex.exec(NativeModules.Location.href);
    let id = '';
    if (results && results[2]){
      id = decodeURIComponent(results[2].replace(/\+/g, " "));
    } 
    switch(id) {
      case '0':
          return "montevideo.jpg";
          break;
      case '1':
          return "new_york.jpg";
          break;
      case '2':
          return "san_francisco.png";
          break;
      case '3':
          return "buenos_aires.jpg";
          break;
      case '4':
          return "cape_town.jpg";
          break;
      default:
          return "tokyo.jpg";
          break;
    }
  }
  render() {
    return (
      <View>
        <Pano source={asset(this.fetchAssetDirection())}/>
      </View>
    );
  }
};

AppRegistry.registerComponent('Wanderlust_VR', () => Wanderlust_VR);
